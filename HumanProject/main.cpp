#include <iostream>
#include "Human.h"
#include "British.h"
#include "Chinese.h"

using namespace std;

void introduce(Human &h1, Human &h2){

   h1.talkTo(h2);
   h2.talkTo(h1);

}


int main()
{

    British Joe("Joe");

    Chinese Yichao("YiChao");

    introduce(Joe, Yichao);


//    joePtr = &Joe; //upcasting



    return 0;
}
