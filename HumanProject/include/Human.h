#ifndef HUMAN_H
#define HUMAN_H
#include <string>
#include <iostream>
using namespace std;

class Human
{
    public:
        Human(const string &name);
        virtual void talkTo(Human &otherHuman);
        string getName();
        virtual ~Human();

    protected:

    private:
        string nameC;
};

#endif // HUMAN_H
